import 'package:flutter/material.dart';

void main() => runApp(BasProject());

class BasProject extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BAS Project',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
        accentColor: Colors.deepOrangeAccent,
        errorColor: Colors.red,
        fontFamily: 'Poppins',
        // fontstyle global
        textTheme: ThemeData.light().textTheme.copyWith(
              // ignore: deprecated_member_use
              title: TextStyle(
                fontFamily: 'Poppins',
                fontWeight: FontWeight.bold,
                fontSize: 23,
                color: Colors.orange,
              ),
              button: TextStyle(
                color: Colors.white,
              ),
            ),
        appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
                // ignore: deprecated_member_use
                title: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[700],
                ),
              ),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      home: UncompletePage(),
    );
  }
}

class UncompletePage extends StatefulWidget {
  @override
  _UncompletePageState createState() => _UncompletePageState();
}

class _UncompletePageState extends State<UncompletePage> {
  // late TopDropDown1 selectedTopDropDown1;

  // final List<TopDropDown1> txTDD1 = [
  //   TopDropDown1(
  //     itemTopDropdown1: 'item 1',
  //   ),
  //   TopDropDown1(
  //     itemTopDropdown1: 'item 2',
  //   ),
  // ];
  String? selected;

  List<String> data = [
    "item 1",
    "item 2",
    "item 3",
  ];

  // List<DropdownMenuItem> generateItems(List<TopDropDown1> txTDD1) {
  //   List<DropdownMenuItem> items = [];
  //   for (var item in txTDD1) {
  //     items.add(DropdownMenuItem(
  //       child: Text(item.itemTopDropdown1),
  //       value: item,
  //     ));
  //   }
  //   return items;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          color: Colors.grey[700],
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: () => null,
        ),
        title: Center(
          child: Text(
            'Uncomplete',
          ),
        ),
        actions: <Widget>[
          IconButton(
            color: Colors.grey[700],
            icon: Icon(
              Icons.menu_outlined,
            ),
            onPressed: () => null,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Flexible(
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      elevation: 5,
                      margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                      child: Container(
                        width: double.infinity,
                        height: 30,
                        child: Padding(
                          padding: EdgeInsets.all(0),
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                isExpanded: true,
                                value: selected,
                                hint: Center(
                                  child: Text("Andhika BP - 21008"),
                                ),
                                onChanged: (value) {
                                  print(value);
                                  setState(() {
                                    selected = value as String;
                                  });
                                },
                                items: data
                                    .map(
                                      (item) => DropdownMenuItem(
                                        value: item,
                                        child: Center(
                                          child: Text(item),
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      elevation: 5,
                      margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                      child: Container(
                        width: double.infinity,
                        height: 30,
                        child: Padding(
                          padding: EdgeInsets.all(0),
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                isExpanded: true,
                                value: selected,
                                hint: Center(
                                  child: Text("21 June 2021"),
                                ),
                                onChanged: (value) {
                                  print(value);
                                  setState(() {
                                    selected = value as String;
                                  });
                                },
                                items: data
                                    .map(
                                      (item) => DropdownMenuItem(
                                        value: item,
                                        child: Center(
                                          child: Text(item),
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                elevation: 5,
                margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: Container(
                        height: 112,
                        // color: Colors.cyanAccent,
                        child: Padding(
                          padding: const EdgeInsets.all(0),
                          child: Container(
                            child: Image.asset(
                              'assets/images/Uncomplete_Assets/PNG/Uncomplete.png',
                              height: 150,
                              width: 200,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        // color: Colors.orange,
                        child: Padding(
                          padding: const EdgeInsets.all(25),
                          child: Center(
                            child: Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: FittedBox(
                                      fit: BoxFit.cover,
                                      child: Text(
                                        "Uncomplete",
                                        style: Theme.of(context)
                                            .textTheme
                                            // ignore: deprecated_member_use
                                            .title,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: FittedBox(
                                      fit: BoxFit.cover,
                                      child: Text("0",
                                          style: Theme.of(context)
                                              .textTheme
                                              // ignore: deprecated_member_use
                                              .title),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Flexible(
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      elevation: 5,
                      margin: EdgeInsets.fromLTRB(20, 3, 20, 3),
                      child: Container(
                        width: double.infinity,
                        height: 30,
                        child: Padding(
                          padding: EdgeInsets.all(0),
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                isExpanded: true,
                                value: selected,
                                hint: Center(
                                  child: Text("2021"),
                                ),
                                onChanged: (value) {
                                  print(value);
                                  setState(() {
                                    selected = value as String;
                                  });
                                },
                                items: data
                                    .map(
                                      (item) => DropdownMenuItem(
                                        value: item,
                                        child: Center(
                                          child: Text(item),
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      elevation: 5,
                      margin: EdgeInsets.fromLTRB(20, 3, 20, 3),
                      child: Container(
                        width: double.infinity,
                        height: 30,
                        child: Padding(
                          padding: EdgeInsets.all(0),
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                isExpanded: true,
                                value: selected,
                                hint: Center(
                                  child: Text("Juni"),
                                ),
                                onChanged: (value) {
                                  print(value);
                                  setState(() {
                                    selected = value as String;
                                  });
                                },
                                items: data
                                    .map(
                                      (item) => DropdownMenuItem(
                                        value: item,
                                        child: Center(
                                          child: Text(item),
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  elevation: 5,
                  margin: EdgeInsets.fromLTRB(20, 3, 20, 3),
                  child: Container(
                    width: double.infinity,
                    height: 30,
                    child: Padding(
                      padding: EdgeInsets.all(0),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton(
                            isExpanded: true,
                            value: selected,
                            hint: Center(
                              child: Text("All Status"),
                            ),
                            onChanged: (value) {
                              print(value);
                              setState(() {
                                selected = value as String;
                              });
                            },
                            items: data
                                .map(
                                  (item) => DropdownMenuItem(
                                    value: item,
                                    child: Center(
                                      child: Text(item),
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 500,
                child: ListView.builder(
                  itemBuilder: (ctx, index) {
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      elevation: 5,
                      margin: EdgeInsets.symmetric(
                        vertical: 8,
                        horizontal: 20,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 10, 10, 0),
                                          child: Text(
                                            "Date : 22 Juni 2021",
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 10, 10, 3),
                                          child: Text(
                                            "System",
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 5, 10, 0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 0, 0, 3),
                                                child: Text(
                                                  "Check In : 08.00",
                                                  style: TextStyle(
                                                      color: Colors.grey[700]),
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 0, 0, 3),
                                                child: Text(
                                                  "Check Out : 18.00",
                                                  style: TextStyle(
                                                      color: Colors.grey[700]),
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 0, 0, 3),
                                                child: Text(
                                                  "Work Hour : 9.00",
                                                  style: TextStyle(
                                                      color: Colors.grey[700]),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 10, 10, 0),
                                          child: Text(
                                            "Reason",
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 10, 10, 0),
                                          child: Text(
                                            "Status",
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 10, 10, 0),
                                          child: Text(
                                            "Action",
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 10, 10, 0),
                                          child: Text(""),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 10, 10, 3),
                                          child: Text(
                                            "Proposed",
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 5, 10, 0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 0, 0, 3),
                                                child: Text(
                                                  "Check In : 08.00",
                                                  style: TextStyle(
                                                      color: Colors.grey[700]),
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 0, 0, 3),
                                                child: Text(
                                                  "Check Out : 18.00",
                                                  style: TextStyle(
                                                      color: Colors.grey[700]),
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 0, 0, 3),
                                                child: Text(
                                                  "Work Hour : 9.00",
                                                  style: TextStyle(
                                                      color: Colors.grey[700]),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        // Padding(
                                        //   padding: const EdgeInsets.all(8.0),
                                        //   child: Text(""),
                                        // ),
                                        // Padding(
                                        //   padding: const EdgeInsets.all(8.0),
                                        //   child: Text(""),
                                        // ),
                                        // Padding(
                                        //   padding: const EdgeInsets.all(8.0),
                                        //   child: Text(""),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  // conter loop
                  // itemCount: transactions.length,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TopDropDown1 {
  final String itemTopDropdown1;

  TopDropDown1({required this.itemTopDropdown1});
}
